import { firebase, firestore } from '~/plugins/firebase'
import uuid from 'uuid/v4'

export const state = () => ({
  /** ガチャ一覧 */
  spotLists: [],
  /** ガチャ */
  spotList: null
})

export const mutations = {
  /** ガチャ一覧を設定します */  
  setSpotLists (state, spotLists) {
    state.spotLists = spotLists
  },
  /** リストの先頭に要素を追加します */
  addSpotListOnTop (state, spotList) {
    state.spotLists.unshift(spotList)
  },
  /** リストを更新します */
  updateSpotList (state, spotList) {
    state.spotLists = spotList
  },
  /** ガチャを設定します */
  setSpotList (state, spotList) {
    state.spotList = spotList
  },
  /** ガチャに行き先を追加します */
  addSpotToSpotList (state, spot) {
    state.spotList.spots.push(spot)
  },
  /** ガチャのお店を更新します */
  updateSpot (state, spot) {
    let targets = state.spotList.spots.filter(s => s.id === spot.id)
    if (targets.length === 0) return
    let target = targets[0]
    Object.keys(spot).forEach(key => target[key] = spot[key])
  },
  /** ガチャのお店を削除します */
  deleteSpot (state, spot) {
    state.spotList.spots = state.spotList.spots.filter(s => s.id !== spot.id)    
  }
 }

export const actions = {
  /** ガチャを登録します */
  async registerSpotList({ commit }, spotListInput) {    
    // 時刻フィールドをセット
    spotListInput.createdAt = firebase.firestore.FieldValue.serverTimestamp()
    spotListInput.updatedAt = firebase.firestore.FieldValue.serverTimestamp()

    // 新しいガチャをFirestoreに作成
    let newSpotList = await firestore.collection('spot-lists').add(spotListInput)

    // 画面表示用のリストに追加
    commit('addSpotListOnTop', spotListInput)

    return newSpotList.id
  },
  /** ガチャを更新します */
  async updateSpotList({ commit }, spotListInput) {
    // 時刻フィールドをセット
    spotListInput.updatedAt = firebase.firestore.FieldValue.serverTimestamp()

    // ガチャを更新
    await firestore.collection('spot-lists').doc(spotListInput.id).set(spotListInput)

    // 画面表示用のリストに追加
    commit('updateSpotList', spotListInput)
  },
  /** ユーザが定義したガチャを一覧取得します */
  async fetchUserDefinedSpotList({ commit }, userId) {
    // Firestoreからユーザに紐づくガチャのみ取得
    let snapshot = await firestore.collection('spot-lists')
      .where('ownerId', '==', userId)
      .orderBy('createdAt', 'desc')
      .get()

    // ストアに格納
    let spotLists = []
    snapshot.forEach(doc => {
      let data = doc.data()
      spotLists.push({
        id: doc.id,
        name: data.name,
        description: data.description,
        ownerId: data.ownerId,
        spots: data.spots,
        createdAt: data.createdAt.toDate(),
        updatedAt: data.updatedAt.toDate()
      })
    })
    commit('setSpotLists', spotLists)
  },
  /** ガチャを削除します */
  async deleteSpotList ({ commit, state }, spotList) {
    await firestore.collection('spot-lists').doc(spotList.id).delete()
    commit('setSpotLists', state.spotLists.filter(s => s.id !== spotList.id))
  },
  /** ガチャIDからガチャを1件取得します */
  async fetchSpotListById({ commit }, spotListId) {
    let doc = await firestore.collection('spot-lists').doc(spotListId).get()
    if (!doc.exists) commit('setSpotList', null)
    let data = doc.data()
    commit('setSpotList', {
      id: doc.id,
      name: data.name,
      description: data.description,
      ownerId: data.ownerId,
      spots: data.spots,
      createdAt: data.createdAt.toDate(),
      updatedAt: data.updatedAt.toDate()
    })
  },
  /** ガチャに行き先を登録します */
  async registerSpot ({ commit, state }, spot) {
    spot.id = uuid()
    commit('addSpotToSpotList', spot)
    await firestore.collection('spot-lists').doc(state.spotList.id).set(state.spotList)
  },
  /** ガチャの行き先を更新します */
  async updateSpot ({ commit, state }, spot) {
    commit('updateSpot', spot)
    await firestore.collection('spot-lists').doc(state.spotList.id).set(state.spotList)
  },
  /** ガチャのお店を削除します */
  async deleteSpot ({ commit, state }, spot) {
    commit('deleteSpot', spot)
    await firestore.collection('spot-lists').doc(state.spotList.id).set(state.spotList)
  }
 }

export const getters = {
  spotLists (state) { return state.spotLists },
  spotList (state) { return state.spotList }
}
