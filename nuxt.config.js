module.exports = {
  mode: 'spa',
  modules: [
    ['bootstrap-vue/nuxt', { css: false }]
  ],
  head: {
    titleTemplate: title => {
      return title ? `${title} | ランチガチャ` : 'ランチガチャ';
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    link: [
      {
        rel: 'stylesheet',
        href: 'https://use.fontawesome.com/releases/v5.5.0/css/solid.css',
        integrity: 'sha384-rdyFrfAIC05c5ph7BKz3l5NG5yEottvO/DQ0dCrwD8gzeQDjYBHNr1ucUpQuljos',
        crossorigin: 'anonymous'
      },
      {
        rel: 'stylesheet',
        href: 'https://use.fontawesome.com/releases/v5.5.0/css/fontawesome.css',
        integrity: 'sha384-u5J7JghGz0qUrmEsWzBQkfvc8nK3fUT7DCaQzNQ+q4oEXhGSx+P2OqjWsfIRB8QT',
        crossorigin: 'anonymous'
      }
    ],
  },
  router: {
    middleware: 'auth'
  },
  plugins: [
    '~/plugins/firebase'
  ],
  css: [
    '@/assets/scss/app.scss'
  ]
}
