import * as firebase from '@firebase/testing'
import fs from 'fs'
import uuid from 'uuid/v4'

export default class FirestoreTestTool {

  constructor (projectIdPrefix) {
    this.projectIdPrefix = projectIdPrefix
    this.projectId = null
    this.rules = fs.readFileSync('firestore.rules', 'utf8')
  }

  async initTestCase () {
    this.projectId = `${this.projectIdPrefix}-${uuid()}`
    await firebase.loadFirestoreRules({
      projectId: this.projectId,
      rules: this.rules
    })
  }

  getFirestore (auth) {
    return firebase.initializeTestApp({
      projectId: this.projectId,
      auth: auth
    })
    .firestore()
  }

  getAdminFirestore () {
    return firebase.initializeAdminApp({
      projectId: this.projectId
    })
    .firestore()
  }

  async cleanUp () {
    return Promise.all(firebase.apps().map(app => app.delete()))
  }
}
