import { assertSucceeds, assertFails } from '@firebase/testing'
import FirestoreTestTool from '../lib/firestore-test-tool'

const testTool = new FirestoreTestTool('users-collection-security-rule-test')

describe('users collection security rule', () => {
  
  beforeEach(async () => {
    await testTool.initTestCase()

    const admin = testTool.getAdminFirestore()
    await admin.collection('users').doc('Test1').set({})
    await admin.collection('users').doc('Test2').set({})
  })

  afterEach(async () => {
    await testTool.cleanUp()
  })

  it('allow read if user is authenticated and authenticated uid equals document id', async () => {
    const firestore = testTool.getFirestore({
      uid: 'Test1'
    })
    await assertSucceeds(firestore.collection('users').doc('Test1').get())
  })

  it('does not allow read if user is unauthenticated', async () => {
    const firestore = testTool.getFirestore(null)
    await assertFails(firestore.collection('users').doc('Test1').get())
  })

  it('does not allow read if authenticated uid is different from document id', async () => {
    const firestore = testTool.getFirestore({
      uid: 'Test1'
    })
    await assertFails(firestore.collection('users').doc('Test2').get())
  })

  it('allow write if user is authenticated and authenticated uid equals document id', async () => {
    const firestore = testTool.getFirestore({
      uid: 'Test1'
    })
    await assertSucceeds(firestore.collection('users').doc('Test1').set({}))
  })

  it('does not allow write if user is unauthenticated', async () => {
    const firestore = testTool.getFirestore(null)
    await assertFails(firestore.collection('users').doc('Test1').set({}))
  })

  it('does not allow write if authenticated uid is different from document id', async () => {
    const firestore = testTool.getFirestore({
      uid: 'Test1'
    })
    await assertFails(firestore.collection('users').doc('Test2').set({}))
  })
})
