import { assertSucceeds, assertFails } from '@firebase/testing'
import FirestoreTestTool from '../lib/firestore-test-tool'

const testTool = new FirestoreTestTool('spot-lists-collection-security-rule-test')

describe('spot lists collection security rule', () => {
  
  beforeEach(async () => {
    await testTool.initTestCase()

    const admin = testTool.getAdminFirestore()
    await admin.collection('spot-lists').doc('List1').set({ ownerId: 'Test1' })
    await admin.collection('spot-lists').doc('List2').set({ ownerId: 'Test2' })
  })

  afterEach(async () => {
    await testTool.cleanUp()
  })

  it('should be readable if user is authenticated and even if authenticated user id is not equal to spot list owner id', async () => {
    const firestore = testTool.getFirestore({
      uid: 'Test1'
    })
    await assertSucceeds(firestore.collection('spot-lists').doc('List2').get())
  })

  it('should not be readable document if user is not authenticated', async () => {
    const firestore = testTool.getFirestore(null)
    await assertFails(firestore.collection('spot-lists').doc('List1').get())
  })

  it('should be creatable if user is authenticated and authenticated user id is equal to owner id', async () => {
    const firestore = testTool.getFirestore({
      uid: 'Test1'
    })
    await assertSucceeds(firestore.collection('spot-lists').add({ ownerId: 'Test1' }))
  })

  it('should not be creatable if user is authenticated but authenticated user id is not equal to owner id', async () => {
    const firestore = testTool.getFirestore({
      uid: 'Test1'
    })
    await assertFails(firestore.collection('spot-lists').add({ ownerId: 'Test2' }))
  })

  it('should not be creatable if user is not authenticated', async () => {
    const firestore = testTool.getFirestore(null)
    await assertFails(firestore.collection('spot-lists').add({ ownerId: 'Test1' }))
  })

  it('should be updatable/deletable if user is authenticated and authenticated user id is equal to both before/after owner id', async () => {
    const firestore = testTool.getFirestore({
      uid: 'Test1'
    })
    await assertSucceeds(firestore.collection('spot-lists').doc('List1').set({ ownerId: 'Test1' }))
    await assertFails(firestore.collection('spot-lists').doc('List1').delete())
  })

  it('should not be updatable/deletable if user is authenticated but authenticated user id is equal to before owner id', async () => {
    const firestore = testTool.getFirestore({
      uid: 'Test1'
    })
    await assertFails(firestore.collection('spot-lists').doc('List2').set({ ownerId: 'Test1' }))
    await assertFails(firestore.collection('spot-lists').doc('List1').delete())
  })

  it('should not be updatable/deletable if user is authenticated but authenticated user id is equal to after owner id', async () => {
    const firestore = testTool.getFirestore({
      uid: 'Test1'
    })
    await assertFails(firestore.collection('spot-lists').doc('List1').set({ ownerId: 'Test2' }))
    await assertFails(firestore.collection('spot-lists').doc('List1').delete())
  })

  it('should not be updatable/deletable if user is not authenticated', async () => {
    const firestore = testTool.getFirestore(null)
    await assertFails(firestore.collection('spot-lists').doc('List1').set({ ownerId: 'Test1' }))
    await assertFails(firestore.collection('spot-lists').doc('List1').delete())
  })
})
